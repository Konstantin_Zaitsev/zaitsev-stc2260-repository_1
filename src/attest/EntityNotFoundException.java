package attest;

public class EntityNotFoundException extends Throwable{
    private int id;
    public EntityNotFoundException() {
    }
    public EntityNotFoundException(int id) {
        this.id = id;
    }
    @Override
    public String getMessage() {
        return "Сущность с id: " + id + " не найдена";
    }
}
