package attest;

import attest.EntityNotFoundException;

public interface EntityRepository<E> {

    E findById(int id) throws EntityNotFoundException, UserNotFoundException;

    void create(E entity);

    void update(E entity);

    void delete(int id);
}

