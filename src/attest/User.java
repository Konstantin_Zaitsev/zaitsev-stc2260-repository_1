package attest;

public class User {
    private Integer id;
    private String name;
    private String surname;
    private Integer age;
    private Boolean isWork;
    public User(String name, String surname, Integer age, Boolean isWork) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isWork = isWork;
    }
    User(Integer id, String name, String surname, Integer age, Boolean isWork) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isWork = isWork;
    }
    public static User fromLine(String line) {
        if (null != line && line.length() > 0) {
            String[] sa = line.split("\\|");
            return new User(Integer.valueOf(sa[0]), sa[1], sa[2],
                    Integer.valueOf(sa[3]), Boolean.valueOf(sa[4]));
        }
        return null;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public Boolean getIsWork() {
        return isWork;
    }
    public void setIsWork(Boolean isWork) {
        this.isWork = isWork;
    }
    public String toString() {
        return id + "|"
                + name + "|"
                + surname + "|"
                + age + "|"
                + isWork;
    }
}
