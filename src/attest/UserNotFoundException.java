package attest;

public class UserNotFoundException extends Throwable {
    private int userId;
    public UserNotFoundException() {
    }
    public UserNotFoundException(int userId) {
        this.userId = userId;
    }
    @Override
    public String getMessage() {
        return "Пользователь с ID: " + userId + " не найден";
    }
}
