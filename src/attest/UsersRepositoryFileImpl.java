package attest;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import attest.UserNotFoundException;

public class UsersRepositoryFileImpl implements EntityRepository<User> {
    private final Path path;
    public UsersRepositoryFileImpl(Path path) {
        this.path = path;
    }
    // поиск пользователя по ID
    public User findById(int userId) throws UserNotFoundException {
        return getUserStream()
                .filter(user -> user.getId() == userId)
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException(userId));
    }
    // создание нового пользователя (запись в файл)
    public void create(User user) {
        try {
            List<User> userList = getUserStream().collect(Collectors.toList());
            int maxId = userList.stream()
                    .mapToInt(User::getId)
                    .reduce(Integer::max).orElse(0);
            user.setId(++maxId);
            userList.add(user);

            Files.write(path,
                    userList.stream().map(User::toString).collect(Collectors.toList()),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    // обновление информации по пользователю
    public void update(User user) {
        if (null == user) {
            return;
        }
        try {
            List<String> updatedUsers = getUserStream()
                    .map(u -> {
                        if (Objects.equals(u.getId(), user.getId()))
                            u = user;
                        return u;
                    })
                    .map(User::toString)
                    .collect(Collectors.toList());

            Files.write(path, updatedUsers, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    // удаление пользователя по id
    public void delete(int deletedUserId) {
        try {
            List<String> usersToString = getUserStream()
                    .filter(user -> user.getId() != deletedUserId)
                    .map(User::toString)
                    .collect(Collectors.toList());

            Files.write(path, usersToString, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    private Stream<User> getUserStream() {
        try {
            return Files.readAllLines(path)
                    .stream()
                    .map(str -> User.fromLine(str));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
