import attest.User;
import attest.UserNotFoundException;
import attest.UsersRepositoryFileImpl;
import java.io.*;
import java.nio.file.Path;


public class Main {
    public static void main(String[] args) {
        Path path = Path.of("Users.txt");
        UsersRepositoryFileImpl entityRepository = new UsersRepositoryFileImpl(path);
        User user1;
        try {
            user1 = entityRepository.findById(1);
            user1.setName("Владимир");
            user1.setAge(27);
            entityRepository.update(user1);
        } catch (UserNotFoundException e) {
            System.out.println(e.getMessage());
        }
        User user2;
        try {
            user2 = entityRepository.findById(2);
            entityRepository.delete(user2.getId());
        } catch (UserNotFoundException e) {
            System.out.println(e.getMessage());
        }
        User user3 = new User("Мария", "Литвинюк", 24, true);
        entityRepository.create(user3);
        System.out.println();
    }
}



