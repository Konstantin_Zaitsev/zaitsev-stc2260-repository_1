import java.util.Arrays;

public class Homework_02 {
    public static void main(String[] args) {
        int[] array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        bubbleSort(array);
        int index = getElementInArray (array, 0);
        System.out.println(index);
        //System.out.println(Arrays.toString(array));
    }

    public static void bubbleSort(int[] array) {
        System.out.println(Arrays.toString(array));
        for (int i = 0; i < array.length-1; i++) {
            for (int j = 0; j < array.length-1; j++)
                if (array[j] < array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
        }
        System.out.println(Arrays.toString(array));
    }
    public static int getElementInArray (int[] array, int value) {
        for (int i = 0; i < array.length; i++){
            if (array[i] == value){
                return i;
            }
        }
        System.out.println("Данное число отсутствует в массиве");
        return -1;

    }

}


