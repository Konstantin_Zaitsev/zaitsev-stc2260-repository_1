package Homework_01;

import java.util.Scanner;

public class Homework_01 {

    public static void main(String[] args) {
        System.out.println("Введите целое число");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        number = Math.abs(number);
        int maxDigit = 0;
        int temp = 0;
        while (number != 0) {
            temp = number % 10;
            if (temp > maxDigit) maxDigit = temp;
            number = number / 10;
        }
        System.out.println("Наибольшая цифра в числе " + maxDigit);
    }
}
