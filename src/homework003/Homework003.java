package homework003;

import java.util.Arrays;

public class Homework003 {
    public static void main(String[] args) {
        int[] array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println("Массив до перемещения всех значимых элементов влево");
        System.out.println(Arrays.toString(array));
        System.out.println("Массив после перемещения всех значимых элементов влево");
        array = moveNotNullNumbersToLeft(array);
        System.out.println(Arrays.toString(array));
        int index = getElementInArray(array, 14);
        System.out.println("Индекс этого числа в массиве: " + index);

    }
    public static int getElementInArray(int[] array, int number){
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return i;
            }
        }
        System.out.println("Число "+ number +" отсутствует в массиве");
        return -1;
    }
    public static int [] moveNotNullNumbersToLeft(int[] array){
        boolean isSorted = false;
        while (!isSorted){
            isSorted = true;
            for (int i=0; i < array.length-1; i++){
                if (array[i] == 0 && array[i+1] != 0){
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    isSorted = false;
                }
            }
        }
        return array;
    }

}


