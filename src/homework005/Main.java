package homework005;

import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        int[] randomArray = CommonRandomArray.createRandomArray(0,100,10);

        System.out.println("Массив чисел: " + Arrays.toString(randomArray));

        int[] arrayWithEvensByAnonymousClass = Numbers.getArrayWithEvensByAnonymousClass(randomArray);
        int[] arrayWithEvensByLambda = Numbers.getArrayWithEvensByLambda(randomArray);
        System.out.println("1.1 Массив четных чисел с использованием анонимного класса: "
                + Arrays.toString(arrayWithEvensByAnonymousClass));
        System.out.println("1.2 Массив четных чисел с использованием лямбда-выражения: "
                + Arrays.toString(arrayWithEvensByLambda));

        int[] arrayOfEvensWithSumOfDigitsInNumber =
                Numbers.getArrayOfEvensWithSumOfDigitsInNumber(randomArray);
        System.out.println("2. Массив четных сумм цифр в числе: "
                + Arrays.toString(arrayOfEvensWithSumOfDigitsInNumber));

        int[] arrayOfEachDigitInNumberIsEven =
                Numbers.getArrayOfEachDigitInNumberIsEven(randomArray);
        System.out.println("3. Массив всех четных цифр в числе: "
                + Arrays.toString(arrayOfEachDigitInNumberIsEven));

        int[] arrayOfPalindromes = Numbers.getArrayOfPalindromes(randomArray);
        System.out.println("4. Массив палиндромов: "
                + Arrays.toString(arrayOfPalindromes));
    }
}

