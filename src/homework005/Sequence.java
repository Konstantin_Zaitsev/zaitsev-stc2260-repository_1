package homework005;

public class Sequence {
    public static int[] filter (int[] array, ByCondition condition) {
        int conditionArrayLength = getConditionArrayLength(array, condition);

        int[] newArray = new int[conditionArrayLength];
        int i = 0;
        for (int item: array) {if (condition.isOk(item)) {
            newArray[i++] = item;
        }
        }

        return newArray;
    }

    private static int getConditionArrayLength(int[] array, ByCondition condition) {
        int i = 0;
        for (int item: array) {
            if (condition.isOk(item)) {
                i++;
            }
        }
        return i;
    }
}