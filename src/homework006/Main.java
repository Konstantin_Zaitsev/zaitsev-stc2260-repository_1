package homework006;

public class Main {
    public static void main(String[] args) {
        Human human = new Human(
                "Зайцев",
                "Константин",
                "Викторович",
                "Мытищи",
                "Колпакова",
                "48",
                "214",
                "Серия: 46 15 Номер: 123455");
        Human human1 = new Human(
                "Конев",
                "Виталий",
                "Андреевич",
                "Путилково",
                "Сходненская",
                "15",
                "114",
                "Серия: 46 11 Номер: 765443");

        System.out.println(human.toString());
        System.out.println(human1.toString());
        System.out.println(human.equals(human1));

    }
}
