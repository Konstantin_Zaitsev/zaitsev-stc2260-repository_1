package homework006;
import java.util.Objects;


public class Human{
    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String home;
    private String flat;
    private String numberPassport;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getNumberPassport() {
        return numberPassport;
    }

    public void setNumberPassport(String numberPassport) {
        this.numberPassport = numberPassport;
    }

    public Human(){}
    public Human(String lastName,
                 String name,
                 String patronymic,
                 String city,
                 String street,
                 String home,
                 String flat,
                 String numberPassport) {
        this.lastName = lastName;
        this.name = name;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.home = home;
        this.flat = flat;
        this.numberPassport = numberPassport;
    }



    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Human human = (Human) o;
        return numberPassport.equals(human.numberPassport);
    }
    public int hashCode() {
        return Objects.hash(numberPassport);
    }
    @Override
    public String toString() {
        return lastName + ' ' +
                name + ' ' +
                patronymic + '\n' +
                "Паспорт:\n" + numberPassport + "\n" +
                "Город " + city + ", " +
                "ул. " + street + ", " +
                "дом " + home + ", " +
                "квартира " + flat + "\n";
    }
}











