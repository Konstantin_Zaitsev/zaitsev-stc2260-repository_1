package org.example;

import attestation.HibernateUtil;
import entity.Students;
import entity.Teachers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class MainManyToMany {
    public static void main(String[] args) {

        try (SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
             Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Teachers teacher_Mathematics = Teachers.builder()
                    .firstName("Irina")
                    .lastName("Filimonova")
                    .subject("Mathematics")
                    .build();

            Teachers teacher_Literature = Teachers.builder()
                    .firstName("Ekaterina")
                    .lastName("Kulikova")
                    .subject("Literature")
                    .build();
            session.save(teacher_Mathematics);
            session.save(teacher_Literature);


            Students student1 = Students.builder()
                    .firstName("Konstantin")
                    .lastName("Zaitsev")
                    .groupNumber("B")
                    .build();


            Students student2 = Students.builder()
                    .firstName("Maria")
                    .lastName("Litviniuk")
                    .groupNumber("B")
                    .build();

            Students student3 = Students.builder()
                    .firstName("Aliona")
                    .lastName("Litviniuk")
                    .groupNumber("C")
                    .build();

            session.save(student1);
            session.save(student2);
            session.save(student3);

            student1.addTeacher(teacher_Literature);
            student1.addTeacher(teacher_Mathematics);
            session.save(student1);

            student2.addTeacher(teacher_Literature);
            student2.addTeacher(teacher_Mathematics);
            session.save(student2);

            student3.addTeacher(teacher_Literature);
            session.save(student3);

            session.getTransaction().commit();

            System.out.println("Обработка информации завершена");
        }
    }
}